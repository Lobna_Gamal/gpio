#include "UART.h"
/*
volatile unsigned char flag=0;
void USART1_IRQHandler()
{
	flag=1;					//receiver interrupt
	
}*/
enum Port{
A=2
,B
,C
,D
,E
,F	};


void UART_Init_API(USART_TypeDef *USART,double BAUDRATE)
{
	if(USART ==  USART1)
	{
	RCC ->APB2ENR |=(1<<14) ; //Enable clock of UART1 and PORTA
  GPIO_INIT_HIGH(GPIOA,0x000004b0,A);//pin9 as output + push pull ..... pin10 as input+floating
	}
	else if(USART ==  USART2) 
	{
    GPIO_INIT_LOW(GPIOA,0x00000b40,A);//pin2 as output + push pull ..... pin1 as input+floating
		RCC -> APB1ENR |=(1<<17);//Enable clock of UART2
	}
	else if(USART ==  USART3)
	{
		  GPIO_INIT_HIGH(GPIOB,0x00004b00,B);//pin10 as output + push pull ..... pin11 as input+floating
			RCC -> APB1ENR |=(1<<18);//Enable clock of UART3
	}
	USART ->CR1 =(1<<13)|(1<<3)|(1<<2);//UE,TE,RE,RXENIE |(1<<5)
	USART ->CR1 &= ~(1<<12);//word lenght 1 start 
	USART ->CR2 &=~(1<<12) & ~(1<<13);
	USART ->BRR = CLOCK/BAUDRATE;
}

void UART_Transmit_Data(USART_TypeDef *USART,unsigned char data)
{
	
	
	  while(!(USART->SR & (1<<7))); //if USART1_SR_TXE is Enable
		//	while((USART1 -> SR & (1<<6)) == 1);													
    USART ->DR = data;


}
void UART_Transmit_String(USART_TypeDef *USART,char * String)
{
	char i=0;
	while(String[i] != '\0')
	{
	UART_Transmit_Data(USART,String[i++]);
	}
}


unsigned char UART_Receive_byte(USART_TypeDef *USART)
{
  while(!(USART->SR & (1<<5)));
	return  (unsigned char)(USART ->DR&0x1ff);
}

void UART_Receive_String(USART_TypeDef *USART,char * string)
{
	char i=0;
	while(UART_Receive_byte(USART) != '\0')
	{
	  string[i]=UART_Receive_byte(USART);
		i++;
	}
	
	
}
