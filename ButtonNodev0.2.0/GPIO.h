#ifndef GPIO_H_
#define GPIO_H_
#include <stdio.h>
#include <stm32f10x.h>

void GPIO_INIT_LOW(GPIO_TypeDef * GPIOX ,int config_Reg,int Port);
void GPIO_INIT_HIGH(GPIO_TypeDef * GPIOX ,int config_Reg,int Port);
unsigned char GPIO_READ(GPIO_TypeDef * GPIOX ,char pin);
void GPIO_SET(GPIO_TypeDef * GPIOX ,char pin);
void GPIO_RESET(GPIO_TypeDef * GPIOX ,char pin);
#endif