#include <stdio.h>
#include <string.h>
#include <stm32f10x.h>
#include "UART.h"
#include "ESPATCommand.h"


void static trimIPMessage(char *, char);

/*
	@brief: Initiate USART , then use it send commands from the STM to the ESP to Initiate the connection,
					set the basic configurations, login to the Network and reclaim an IP
	@parameter: None
	@return: void
*/
void ESP_Init(){
	
	char ipReplyMessage[30];
	
	//AT+CWJAP="Lab 1","MY@w1y@@"
	char ConnectToWifiBaseMSG[35] = "AT+CWJAP=";
	
	//AT+CIPSTA="192.168.1.110"
	char RequestIPBaseMSG[30] = "AT+CIPSTA";

	UART_Init_API(USARTX,BOUD_RATE);
	UART_Transmit_String(USARTX,"AT\r\n");//start connection
	UART_Transmit_String(USARTX,"AT+CWMODE=1\r\n");//station mode

	strcat(ConnectToWifiBaseMSG, USERNAME);
	strcat(ConnectToWifiBaseMSG, ",");
	strcat(ConnectToWifiBaseMSG, PASSWORD);
	strcat(ConnectToWifiBaseMSG, "\r\n");
	UART_Transmit_String(USARTX,ConnectToWifiBaseMSG);//login to network
	
	UART_Transmit_String(USARTX,"AT+CIPMUX=1\r\n"); //Multiple
	
	strcat(RequestIPBaseMSG,NODEIP); //add the ip yto the request ip message
	strcat(RequestIPBaseMSG,"\r\n");
	do{
	
		UART_Transmit_String(USARTX,RequestIPBaseMSG);//get ip
		UART_Transmit_String(USARTX,"AT+CIFSR\r\n");//check ip
		UART_Receive_String(USARTX,ipReplyMessage);//get respond to check ip
		trimIPMessage(ipReplyMessage,',');
	
	}while(!(strcmp(ipReplyMessage,NODEIP))); //if couldn't get ip, repeat

}

/*
	@brief: Configure ESP as a Client, and Connect it to the Server
	@parameter: None
	@return: void
*/
void ESP_Configure_Client(){
	
	//AT+CIPSTART="TCP","192.168.1.107",80
	char conectToServerBaseMessage[40] = "AT+CIPSTART=\"TCP\",";
	strcat(conectToServerBaseMessage,SERVER_ID);
	strcat(conectToServerBaseMessage,SERVER_PORT);
	strcat(conectToServerBaseMessage,"\r\n");
	UART_Transmit_String(USARTX,conectToServerBaseMessage);
}

/*
	@brief: Configure ESP as a Server, set it's port
	@parameter: None
	@return: void
*/
void ESP_Configure_Server(){
	//AT+CIPSERVER=1,80
	char InitServerBaseMessage[20] = "AT+CIPSERVER=1,";
	strcat(InitServerBaseMessage,SERVER_PORT);
	strcat(InitServerBaseMessage,"\r\n");
	UART_Transmit_String(USARTX,InitServerBaseMessage);
}


/*
	@brief: send a messege through ESP by transmiting the sending command with the USART_Transmit_String function
	@parameter: a string of characters
	@return: void
*/
void Transmit_to_ESP(char * message){
	UART_Transmit_String(USARTX, "AT+CIPSEND=6\r\n"); //length of message
	UART_Transmit_String(USARTX, message);//content of the message
}


/*
	@brief: Reads the received message using the USART_Recieve_String function
	@parameter: None
	@return: void
*/
void Recieve_from_ESP(char * message){
	//+IPD,channel,message_size:Message_content
	UART_Receive_String(USARTX,message);
	trimIPMessage(message,':');
}

/*
	@brief: sends the closing command to the ESP to terminate the connection
	@parameter: None
	@return: void
*/
void ESP_Client_Close_Connection(){
	UART_Transmit_String(USARTX,"AT+CIPCLOSE\r\n");	
}


/*
	@brief: a function used to trim the first part of the string determined by a given delimter
	@parameter: a string of characters
	@return: void
*/
void static trimIPMessage(char * string, char character){
	int i = 0, j = 0, flag =0;
	
	while(string[i+1] != '\0'){
	    
		if(string[i] == character){
		    flag =1;
		}
		
		if(flag){
		  	string[j] = string [i+1];
			j++;  
		}
		
		i++;
	}
	string[j] = '\0';
}