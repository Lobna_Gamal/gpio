

#ifndef  UART_H_
#define  UART_H_
#include "GPIO.h"
#define   CLOCK    8000000

void UART_Init_API(USART_TypeDef * USART,double);
void UART_Transmit_Data(USART_TypeDef *, unsigned char data);
void UART_Transmit_String(USART_TypeDef *, char * String);
unsigned char UART_Receive_byte(USART_TypeDef *);
void UART_Receive_String(USART_TypeDef *,char * string);

#endif