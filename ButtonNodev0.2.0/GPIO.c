#include "GPIO.h"

/////////////////////init/////////////////////////////

void GPIO_INIT_LOW(GPIO_TypeDef * GPIOX ,int config_Reg,int Port){
	
	RCC ->APB2ENR |=(1<<Port);
	GPIOX->CRL=config_Reg;
	
	
}

void GPIO_INIT_HIGH(GPIO_TypeDef * GPIOX ,int config_Reg,int Port){
	
	RCC ->APB2ENR |=(1<<Port);
	GPIOX->CRH=config_Reg;
	
}

//////////////////////////////read//////////////////////////////////////////
unsigned char GPIO_READ(GPIO_TypeDef * GPIOX ,char pin){
	
	return (GPIOX->IDR)& (1<< pin);
	
}

//////////////////////////////set/////////////////////////////////////////////
void GPIO_SET(GPIO_TypeDef * GPIOX ,char pin){
	
	GPIOX->BSRR|=(1<<pin);
	
}
//////////////////////////////////////////////////////////
//////////////////////////////reset///////////////////////////////////////

void GPIO_RESET(GPIO_TypeDef * GPIOX ,char pin){
	
	GPIOX->BSRR &=~(1<<(pin+16));
	
}






