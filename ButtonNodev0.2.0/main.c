#include "Button.h"
#include "ESPATCommand.h"
#include "GPIO.h"
/*SYSTEM INIT*/
void SystemInit(){
	
}
void delay()
{
	 volatile int i;
	  for(i=0;i<100000;i++)
		{
			
		}
	
}
enum Port{
A=2
,B
,C
,D
,E
,F	};
/*All Defines and Macros*/
#define      ToggleStatus        ( (status) ^ (1<<0) )


/*Global Variables*/
volatile unsigned char status =0;

/*Functions Prototypes*/
void buttonSend(unsigned char  status);


/*main function*/
int main()
{
	 Button_Init();
	 ESP_Init();
	 ESP_Configure_Client();
	 GPIO_INIT_HIGH(GPIOC,0X00300000,C);
	
 /*program*/
	while(1)
	{
	
	  if(Button_Status())
		{
			//GPIOC->ODR^=(1<<13);
			status = ToggleStatus; //status= status^(1<<0) ... status= 00000000 >> 00000001
			buttonSend(status);
			delay();
		}
	
	}
	
	return 0;
}

void buttonSend(unsigned char status){
	
	if(status == 1)
	{
		GPIOC->ODR^=(1<<13);
		
		 Transmit_to_ESP("B,1\r\n");
	}
	else if (status == 0)
	{
		GPIOC->ODR^=(1<<13);
		    
		 Transmit_to_ESP("B,0\r\n");
	}
}