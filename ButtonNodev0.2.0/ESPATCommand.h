#ifndef ESPATCommand_H_
#define ESPATCommand_H_

#define USERNAME     "\"Lab 1\""
#define PASSWORD     "\"MY@w1y@@\""
#define NODEIP       "\"192.168.1.7\""
#define SERVER_ID    "\"192.168.1.109\""
#define SERVER_PORT  "5000"

#define USARTX USART1
#define BOUD_RATE 9600

void ESP_Init();
void ESP_Configure_Client();
void ESP_Configure_Server();
void Transmit_to_ESP();
void Recieve_from_ESP();

#endif